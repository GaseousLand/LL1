package LL1.tools

import std.collection.*

// 插入Map
public func insert_Map(map: HashMap<String, HashSet<String>>, Vn: String, set: HashSet<String>) {
    // 判断Vn是否已经存在，若不存在则插入
    if (!map.contains(Vn)) {
        map.put(Vn, set)
    } else {
        map[Vn].putAll(set)
    }
}

// 递归求解FIRST集
public func First_next(parser: Parser, Vn: String, is_record: HashMap<String, Bool>): Unit {
    // 判断Vn是否已经求解过
    if (is_record[Vn]) {
        return
    }
    var rightList = parser.Map[Vn]
    for (right in rightList) {
        // 终结符号或者"ε"直接加入
        if (!parser.Vn.contains(right[0]) || right[0] == "ε") {
            if (!parser.First.contains(Vn)) {
                parser.First.put(Vn, HashSet(right[0]))
            } else {
                parser.First[Vn].put(right[0])
            }
        } else {
            // 判断right[0]是否已经求解过
            if (!is_record[right[0]]) {
                First_next(parser, right[0], is_record)
            }
            // 判断是否含有空串
            if (parser.First[right[0]].contains("ε")) {
                var temp = parser.First[right[0]].clone()
                temp.remove("ε")
                parser.First[Vn].putAll(temp)
                // 若有空串，则继续判断下一个符号
                for (i in 1..right.size) {
                    if (!parser.Vn.contains(right[i])) {
                        parser.First[Vn].put(right[i])
                        break
                    } else {
                        First_next(parser, right[i], is_record)
                        if (!parser.First[right[i]].contains("ε")) {
                            var temp = parser.First[right[i]].clone()
                            parser.First[Vn].putAll(temp)
                            break
                        } else {
                            var temp = parser.First[right[i]].clone()
                            temp.remove("ε")
                            parser.First[Vn].putAll(temp)
                            continue
                        }
                    }
                }
            } else {
                // 没有空串，直接加入
                var temp = parser.First[right[0]].clone()
                parser.First[Vn].putAll(temp)
            }
        }
    }
    // 设置为已求解
    is_record[Vn] = true
}

// 求FIRST集
public func First_get(parser: Parser) {
    // 初始化求解记录
    var is_record = HashMap<String, Bool>()
    for (v in parser.Vn) {
        is_record[v] = false
        parser.First.put(v, HashSet())
    }
    for (v in parser.Vn) {
        First_next(parser, v, is_record)
    }
    println("-------------------")
    println("First集")
    for ((k, v) in parser.First) {
        println("${k}: ${v}")
    }
}

// 递归求解FOLLOW集
public func Follow_next(parser: Parser, Vn: String, is_record: HashMap<String, Bool>): Unit {
    // 判断Vn是否已经求解过
    if (is_record[Vn]) {
        return
    }
    // 遍历所有产生式，找到右侧包含Vn的产生式
    for (v in parser.Vn) {
        var rightList = parser.Map[v]
        for (right in rightList) {
            if (right.contains(Vn)) {
                var index = 0
                for (i in 0..right.size) {
                    if (right[i] == Vn) {
                        index = i
                    }
                }
                // 在末尾位置
                if (index == right.size - 1 && v != Vn) {
                    Follow_next(parser, v, is_record)
                    parser.Follow[Vn].putAll(parser.Follow[v])
                // 紧接着的为终结符
                } else if (index < right.size - 1) {
                    // 下一个符号是为终结符
                    if (!parser.Vn.contains(right[index + 1])) {
                        parser.Follow[Vn].put(right[index + 1])
                    } else { // 下一个符号为非终结符
                        // First集中没有空串，把First集加入Follow集
                        if (!parser.First[right[index + 1]].contains("ε")) {
                            let first = parser.First[right[index + 1]].clone()
                            parser.Follow[Vn].putAll(first)
                        } else {
                            // 转向空串后的下一个元素
                            var pos = index + 1
                            var first = parser.First[right[pos]].clone()
                            // println("pos:${pos} next:${right[pos]} first:${first}")
                            while (first.contains("ε")) {
                                // println("pos:${pos} next:${right[pos]} first:${first}")
                                first.remove("ε")
                                parser.Follow[Vn].putAll(first)
                                if (!parser.Vn.contains(right[pos])) {
                                    // 下一个符号为终结符，加入Follow集后退出循环
                                    parser.Follow[Vn].put(right[pos])
                                    break
                                }
                                if (pos + 1 == right.size) {
                                    break
                                } else {
                                    pos += 1
                                    first = parser.First[right[pos]].clone()
                                }
                            }
                            // 在倒数第二个位置
                            if (pos + 1 == right.size) {
                                first = parser.First[right[pos]].clone()
                                if (first.contains("ε")) {
                                    Follow_next(parser, v, is_record) // 递归调用求解FOLLOW集
                                    parser.Follow[Vn].putAll(parser.Follow[v])
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    // 设置为已求解
    is_record[Vn] = true
}

// 求FOLLOW集
public func Follow_get(parser: Parser) {
    // 初始化求解记录
    var is_record = HashMap<String, Bool>()
    for (v in parser.Vn) {
        is_record[v] = false
        // 把$加入开始符号的follow集
        if (v == parser.start) {
            parser.Follow.put(v, HashSet("$"))
        } else {
            parser.Follow.put(v, HashSet())
        }
    }
    for (v in parser.Vn) {
        Follow_next(parser, v, is_record)
    }
    println("-------------------")
    println("Follow集")
    for ((k, v) in parser.Follow) {
        println("${k}: ${v}")
    }
}
